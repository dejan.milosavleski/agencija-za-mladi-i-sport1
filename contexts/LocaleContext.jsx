import * as React from 'react';
import { useRouter } from 'next/router';
import { goTo } from '../utils';

const MK = 'mk';
const SQ = 'sq';
const EN = 'en';

export const locales = { MK, SQ, EN };
export const LocaleContext = React.createContext({});
LocaleContext.displayName = 'LocaleContext';

export const WithLocaleContext = ({ children }) => {
  const router = useRouter();
  const { locale: routerLocale } = router;
  const [locale, setLocale] = React.useState(routerLocale);

  const changeLocale = React.useCallback(
    (language) => {
      if (language !== locale) {
        const localeChangedPath = `/${language}/home`;
        goTo(localeChangedPath);
        setLocale(language);
      }
    },
    [locale]
  );

  return <LocaleContext.Provider value={{ locale, changeLocale }}>{children}</LocaleContext.Provider>;
};
