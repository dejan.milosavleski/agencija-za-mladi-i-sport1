import React from 'react';
import { useRouter } from 'next/router';
import styled from '@emotion/styled';

// Components
import { CardWithText } from '../common/CardWithText';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { ScrollEndMessage } from '../common/ScrollEndMessage';
import { LoaderMessage } from '../common/LoaderMessage';
import { StyledInfiniteScroll } from '../common/StyledElements';

// Utils
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';
import { ErrorPage } from '../ErrorPage';
import { useTranslation } from 'next-i18next';

const StyledRow = styled(Row)`
  justify-content: space-between;
`;

export const NewsList = ({ initialNews }) => {
  const [newsItems, setNewsItems] = React.useState(initialNews);
  const [page, setPage] = React.useState(2);
  const [hasMoreNews, setHasMoreNews] = React.useState(true);
  const router = useRouter();
  const { category } = router.query;
  const { t } = useTranslation('common');

  React.useEffect(() => {
    setNewsItems(initialNews);
  }, [initialNews]);

  const fetchMoreNews = async () => {
    const fetchUrl = category
      ? `https://ams.altius.digital/wp-json/v1/posts/?limit=6&page=${page}&category=${category}`
      : `https://ams.altius.digital/wp-json/v1/posts/?limit=6&page=${page}`;

    const res = await fetch(fetchUrl);

    const news = await res.json();

    const updatedNews = newsItems.concat(news);
    setNewsItems(updatedNews);
    setPage(page + 1);
    setHasMoreNews(!_isEmpty(news));
  };

  return _isEmpty(initialNews) ? (
    <ErrorPage message={t('noData')} />
  ) : (
    <StyledInfiniteScroll
      dataLength={_get(newsItems, 'length', 0)}
      next={fetchMoreNews}
      hasMore={hasMoreNews}
      loader={<LoaderMessage />}
      endMessage={<ScrollEndMessage />}
    >
      <StyledRow>
        {_map(newsItems, (ni) => (
          <Col xs={12} sm={6} md={5}>
            <CardWithText imageUrl={ni.imageUrl} title={ni.title} subtitle={ni.excerpt} postName={ni.slug} white />
          </Col>
        ))}
        <Col xs={0} md={3} />
      </StyledRow>
    </StyledInfiniteScroll>
  );
};
