import React from 'react';
import { useTranslation } from 'next-i18next';
import styled from '@emotion/styled';

// Components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import { Categories } from '../../common/categories/Categories';
import { NewsContent } from '../NewsContent';
import { RelatedNews } from '../../projects-page/RelatedNews';
import { StyledContainer } from '../NewsAndCategories';
import Image from 'react-bootstrap/Image';
import { NoData } from '../../common/NoData';

// Utils
import _get from 'lodash/get';
import { notFoundObject } from '../../../contexts/constants';
import { breakpoints } from '../../../styles/theme';

// Contexts
import { LocaleContext } from '../../../contexts/LocaleContext';

const StyledImage = styled(Image)`
  width: 100%;
  min-height: 23vw;
  margin-bottom: 30px;
  @media ${breakpoints.xl} {
    max-height: 40vw;
  }

  @media ${breakpoints.md} {
    width: 80%;
    min-height: 33vw;
  }
`;

export const SingleNewsItem = ({ slug, categories, currentCategory }) => {
  const { t } = useTranslation('common');
  const relatedNewsLabel = t('relatedNews');
  const { locale } = React.useContext(LocaleContext);

  const [newsItem, setNewsItem] = React.useState({});

  const fetchAndSetData = async () => {
    const pageData = await fetch(`https://ams.altius.digital/wp-json/v1/post/${slug}`)
      .then((response) => response.json())
      .catch((e) => notFoundObject);
    setNewsItem(pageData);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  React.useEffect(() => fetchAndSetData(), [slug]);

  const related_content = _get(newsItem, 'related_content');
  const imageUrl = _get(newsItem, 'imageUrl');

  const notFound = _get(newsItem, 'data.status') === 404;

  return (
    <StyledContainer>
      <>
        <Row>
          <Col xs={12} lg={9}>
            {notFound ? (
              <NoData />
            ) : (
              <>
                <StyledImage src={imageUrl} alt="sliderImage" fluid standalone />
                <NewsContent newsItem={newsItem} />
              </>
            )}
          </Col>
          <Col xs={{ span: 12, order: 'first' }} lg={{ span: 3, order: 'last' }}>
            <Categories
              categories={categories}
              currentCategory={currentCategory}
              firstPageName="articles"
              locale={locale}
            />
          </Col>
        </Row>
        {!notFound && (
          <Row>
            <RelatedNews content={related_content} title={relatedNewsLabel} />
          </Row>
        )}
      </>
    </StyledContainer>
  );
};
