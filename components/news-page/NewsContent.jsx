import React from 'react';
import styled from '@emotion/styled';
import _get from 'lodash/get';
import { StyledHtml } from '../common/StyledHtml';

const Title = styled.div`
  font-size: 36px;
  font-family: StobiSans-Bold, sans-serif;
  margin-bottom: 10px;
`;

export const NewsContent = ({ newsItem }) => {
  const title = _get(newsItem, 'title');
  const content = _get(newsItem, 'content');

  return (
    <div aria-labelledby="news-content-title">
      <Title id="news-content-title">{title}</Title>
      <StyledHtml dangerouslySetInnerHTML={{ __html: content }} />
    </div>
  );
};
