import React from 'react';
import styled from '@emotion/styled';

// Components
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image';
import { Categories } from '../common/categories/Categories';
import Carousel from 'react-bootstrap/Carousel';
import Link from 'next/link';
import { SliderSubtitle, SliderTitle } from '../common/StyledTypography';
import { NewsList } from './NewsList';

// Utils
import _map from 'lodash/map';
import { goTo } from '../../utils';
import { breakpoints } from '../../styles/theme';

// Context
import { LocaleContext } from '../../contexts/LocaleContext';

const CarouselItem = styled(Carousel.Item)`
  width: 100%;
  text-align: left;
  @media ${breakpoints.md} {
    text-align: center;
  }
`;

const StyledImage = styled(Image)`
  height: 100%;
  width: 100%;
  min-height: 24vw;
  :hover {
    cursor: ${(props) => !props.standalone && 'pointer'};
  }

  @media ${breakpoints.xl} {
    max-height: 40vw;
  }

  @media ${breakpoints.md} {
    width: 80%;
    min-height: 33vw;
  }
`;

export const StyledContainer = styled(Container)`
  margin-top: 40px;
  margin-bottom: 40px;
`;

export const NewsAndCategories = ({ firstTwoNews, categories, currentCategory, firstPageName, restOfNews }) => {
  const { locale } = React.useContext(LocaleContext);

  return (
    <StyledContainer>
      <Row>
        <Col xs={12} md={9}>
          {firstTwoNews && (
            <>
              <Carousel nextIcon={''} prevIcon={''}>
                {_map(firstTwoNews, (si) => (
                  <CarouselItem>
                    <StyledImage
                      src={si.imageUrl}
                      alt="sliderImage"
                      fluid
                      onClick={() => goTo(`/${locale}/article/${si.slug}`)}
                    />
                    <Carousel.Caption>
                      <SliderTitle>
                        <Link href={`/article/${si.slug}`} locale={locale}>
                          {si.title}
                        </Link>
                      </SliderTitle>
                      <SliderSubtitle>{si.excerpt}</SliderSubtitle>
                    </Carousel.Caption>
                  </CarouselItem>
                ))}
              </Carousel>
              <NewsList initialNews={restOfNews} />
            </>
          )}
        </Col>
        <Col xs={{ span: 12, order: 'first' }} md={{ span: 3, order: 'last' }}>
          <Categories
            categories={categories}
            currentCategory={currentCategory}
            firstPageName={firstPageName}
            locale={locale}
          />
        </Col>
      </Row>
    </StyledContainer>
  );
};
