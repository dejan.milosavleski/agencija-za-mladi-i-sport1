import React from 'react';
import styled from '@emotion/styled';
import { breakpoints } from '../../styles/theme';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Image } from 'react-bootstrap';

const StyledRow = styled(Row)`
  padding-right: 20px;
`;

const StyledImage = styled(Image)`
  margin-bottom: 20px;
  margin-top: 30px;
`;

const RightCol = styled(Col)`
  align-self: center;
`;

const Title = styled.div`
  font-size: 2.2vw;
  font-family: StobiSans-Medium, sans-serif;
  @media ${breakpoints.lg} {
    font-size: 3.3vw;
  }
  @media ${breakpoints.sm} {
    font-size: 5.5vw;
  }
`;

const Subtitle = styled.div`
  font-size: 1.3vw;
  @media ${breakpoints.lg} {
    font-size: 2.3vw;
  }
  @media ${breakpoints.md} {
    font-size: 2vw;
  }
  @media ${breakpoints.sm} {
    font-size: 3.5vw;
  }
`;

const Text = styled.div`
  padding-top: 20px;
  font-size: 0.95vw;
  @media ${breakpoints.lg} {
    font-size: 1.5vw;
  }
  @media ${breakpoints.md} {
    font-size: 1.8vw;
  }
  @media ${breakpoints.sm} {
    font-size: 2.5vw;
  }
`;

export const ImageWithText = ({ imageUrl, title, subtitle, text }) => {
  return (
    <Row>
      <Col xs={12} sm={6}>
        <StyledImage src={imageUrl} alt="person" fluid />
      </Col>
      <RightCol xs={12} sm={6}>
        <StyledRow aria-labeledby="title">
          <Title id="title">{title}</Title>
        </StyledRow>
        <StyledRow>
          <Subtitle>{subtitle}</Subtitle>
        </StyledRow>
        <StyledRow>
          <Text>{text}</Text>
        </StyledRow>
      </RightCol>
    </Row>
  );
};
