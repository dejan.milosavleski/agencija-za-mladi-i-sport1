import styled from '@emotion/styled';
import { breakpoints } from '../../styles/theme';

export const TitleTypography = styled.div`
  font-size: 32px !important;
  @media ${breakpoints.md} {
    font-size: 28px !important;
  }

  @media ${breakpoints.sm} {
    font-size: 22px !important;
  }

  @media ${breakpoints.xs} {
    font-size: 15px !important;
  }
`;

export const SliderTitle = styled.div`
  text-align: left;
  font-size: max(min(0.5vw, 0.5em), 1.6em, 1.6rem);
  font-family: StobiSans-Medium, sans-serif;
  @media ${breakpoints.lg} {
    font-size: 18px;
  }
  @media ${breakpoints.md} {
    font-size: 16px;
  }
  @media ${breakpoints.sm} {
    font-size: 12px;
  }
`;

export const SliderSubtitle = styled.div`
  text-align: left;
  font-size: max(min(0.5vw, 0.5em), 1em, 1rem);
  margin-bottom: 8px;
  @media ${breakpoints.lg} {
    font-size: 16px;
  }
  @media ${breakpoints.md} {
    font-size: 12px;
  }
  @media ${breakpoints.sm} {
    font-size: 10px;
  }
`;

export const PageTitle = styled.div`
  font-size: 20px;
  font-family: StobiSans-Light, sans-serif;
  margin: 10px 0;
`;
