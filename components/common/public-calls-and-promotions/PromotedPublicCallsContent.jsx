import React from 'react';
import ListGroup from 'react-bootstrap/ListGroup';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../../styles/theme';
import _map from 'lodash/map';
import Image from 'next/image';
import arrow from '../../../assets/icons/arrow-red.svg';
import Link from 'next/link';
import { Flex } from '../../common/StyledHtml';
import { LocaleContext } from '../../../contexts/LocaleContext';
import { StyledIconImage } from '../StyledButton';

const Content = styled.div`
  min-height: 23vw;
  max-width: 380px;
  text-align: left;
  background-color: ${theme.palette.lightGray};
  @media ${breakpoints.xl} {
    max-width: none;
  }
`;

export const Title = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
`;

export const Divider = styled.hr`
  border: 3px solid ${theme.palette.primaryRed};
  width: 60px;
  opacity: 1;
`;

const StyledItem = styled(ListGroup.Item)`
  min-height: 50px;
  background-color: ${theme.palette.lightGray};
  display: ${(props) => (props.first ? '' : 'inline-flex')};
  justify-content: ${(props) => (props.first ? 'none' : 'space-between')};
`;

const StyledList = styled(ListGroup)`
  padding: 10px;
`;

export const PromotedPublicCallsContent = ({ promotedPublicCalls, title, seeAll, page }) => {
  const { locale } = React.useContext(LocaleContext);
  return (
    <Content>
      <StyledList variant="flush">
        <StyledItem first="first">
          <Title>{title}</Title>
          <Divider />
        </StyledItem>
        {_map(promotedPublicCalls, (item) => (
          <StyledItem key={item.ID}>
            <Flex flex={1} title={item.title} paddingtop={item.title.length < 40 ? 15 : 0}>
              {item.title}
            </Flex>
            <Link href={`/${page}/${item.post_name}`} locale={locale}>
              <StyledIconImage>
                <Image src={arrow} alt="arrow" width={15} height={15} />
              </StyledIconImage>
            </Link>
          </StyledItem>
        ))}
        <StyledItem>
          <Link href={'/public-calls'} locale={locale}>
            {seeAll}
          </Link>
        </StyledItem>
      </StyledList>
    </Content>
  );
};
