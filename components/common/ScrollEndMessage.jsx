import React from 'react';
import { useTranslation } from 'next-i18next';

export const ScrollEndMessage = ({ message }) => {
  const { t } = useTranslation('common');

  const displayMessage = message || t('endOfScrollDefault');
  return (
    <p style={{ textAlign: 'center' }}>
      <b>{displayMessage}</b>
    </p>
  );
};
