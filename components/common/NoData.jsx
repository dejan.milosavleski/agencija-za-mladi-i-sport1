import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';
import { useTranslation } from 'next-i18next';
import _toUpper from 'lodash/toUpper';

const Container = styled.div`
  height: 100%;
  width: 100%;
  text-align: -webkit-center;
  padding-top: 10vw;
  font-size: 24px;
  @media ${breakpoints.sm} {
    font-size: 18px;
  }
  font-family: StobiSans-Bold, sans-serif;
  color: ${theme.palette.navGray};
  padding-left: 10px;
  text-align: center;
`;

export const NoData = () => {
  const { t } = useTranslation('common');
  const label = t('noData');

  return <Container>{_toUpper(label)}</Container>;
};
