import React from 'react';
import _map from 'lodash/map';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';

// Components
import Link from 'next/link';
import Image from 'next/image';
import ListGroup from 'react-bootstrap/ListGroup';
import { Flex } from './StyledHtml';
import { StyledIconImage } from './StyledButton';

// Context
import { LocaleContext } from '../../contexts/LocaleContext';

// icons
import arrow from '../../assets/icons/arrow-red.svg';

const Content = styled.div`
  min-height: 23vw;
  max-width: ${(props) => (props.paddingleft || props.paddingright ? '' : ' 380px')};
  padding-left: ${(props) => props.paddingleft}px;
  padding-right: ${(props) => props.paddingright}px;
  background-color: ${(props) => (props.white ? 'white' : theme.palette.lightGray)};
`;

const Title = styled.div`
  color: ${(props) => (props.noborder ? theme.palette.primaryRed : theme.palette.navGray)};
  font-size: 22px;
  margin-bottom: ${(props) => props.noborder && '20px'};
  font-family: StobiSans-Medium, sans-serif;
`;

const Divider = styled.hr`
  border: 3px solid ${theme.palette.primaryRed};
  width: 60px;
  opacity: 1;
`;

const StyledItem = styled(ListGroup.Item)`
  min-height: 50px;
  background-color: ${(props) => (props.white ? 'white' : theme.palette.lightGray)};
  display: ${(props) => (props.first ? '' : 'inline-flex')};
  justify-content: ${(props) => (props.first ? 'none' : 'space-between')};
  align-self: ${(props) => props.center};
  text-decoration: ${(props) => props.last && 'underline'};
`;

const StyledList = styled(ListGroup)`
  padding: 10px;
`;

export const NewsItemList = ({ title, endItem, items, noBorder, alignCenter, paddingLeft, paddingRight }) => {
  const { locale } = React.useContext(LocaleContext);

  return (
    <Content white={alignCenter} paddingleft={paddingLeft} paddingright={paddingRight}>
      <StyledList variant="flush">
        <StyledItem first="first" white={alignCenter}>
          <Title noborder={noBorder}>{title}</Title>
          {!noBorder && <Divider />}
        </StyledItem>
        {_map(items, (item) => {
          const verticalAlign = item.title.length < 45;
          return (
            <StyledItem key={item.ID} white={alignCenter}>
              <Flex flex={1} title={item.title} paddingtop={verticalAlign ? 13 : 0}>
                {item.title}
              </Flex>
              <Link href={`public-call/${item.post_name}`} locale={locale}>
                <StyledIconImage>
                  <Image src={arrow} alt="arrow" width={15} height={15} />
                </StyledIconImage>
              </Link>
            </StyledItem>
          );
        })}
        {items && (
          <StyledItem center={alignCenter ? 'center' : ''} white={alignCenter} last="last">
            <Link href={endItem.href} locale={locale}>
              {endItem.text}
            </Link>
          </StyledItem>
        )}
      </StyledList>
    </Content>
  );
};
