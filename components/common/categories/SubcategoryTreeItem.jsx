import React from 'react';
import Link from 'next/link';
import { Icon, SubCategoryItem, SubLinkContainer } from './CategoryTreeItem';
import Image from 'next/image';
import arrowUp from '../../../assets/icons/arrow-up.png';
import arrowDown from '../../../assets/icons/arrow-down.png';
import _isNil from 'lodash/isNil';
import _isEmpty from 'lodash/isEmpty';

export const SubcategoryTreeItem = ({
  items,
  subcategory,
  itemSelected,
  subcategoryUrl,
  currentSubcategory,
  locale
}) => {
  const [showTernaryTree, setShowTernaryTree] = React.useState(null);

  // TODO smihail 2021-10-12: implement if third level categories needed
  const [ternarySelected, setTernarySelected] = React.useState(null);

  React.useEffect(() => {
    if (_isNil(showTernaryTree)) {
      setShowTernaryTree(!_isEmpty(currentSubcategory));
    }
  }, [currentSubcategory, showTernaryTree]);

  const arrowIcon = showTernaryTree ? arrowUp : arrowDown;

  const iconSize = showTernaryTree ? 17 : 20;

  return (
    <>
      <SubCategoryItem key={subcategory.term_id}>
        <SubLinkContainer itemSelected={itemSelected}>
          <Link href={`${subcategoryUrl}/${subcategory.slug}`} locale={locale}>
            {subcategory.name}
          </Link>
        </SubLinkContainer>
        <Icon onClick={() => setShowTernaryTree(!showTernaryTree)}>
          <Image src={arrowIcon} alt="left" width={iconSize} height={iconSize} />
        </Icon>
      </SubCategoryItem>
      {showTernaryTree &&
        items.map((categoryChild) => (
          <SubCategoryItem key={categoryChild.term_id}>
            <SubLinkContainer itemSelected={categoryChild.slug === ternarySelected} ternary>
              <Link href={`${subcategoryUrl}/${categoryChild.slug}`} locale={locale}>
                {categoryChild.name}
              </Link>
            </SubLinkContainer>
          </SubCategoryItem>
        ))}
    </>
  );
};
