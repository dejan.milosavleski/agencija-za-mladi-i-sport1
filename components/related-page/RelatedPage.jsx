import React from 'react';
import styled from '@emotion/styled';
import _get from 'lodash/get';
import { notFoundObject } from '../../contexts/constants';

// Components
import { SingleTitleArea } from '../single-item-page/SingleTitleArea';
import { ImageContainer } from '../single-item-page/SingleItemMainArea';
import { StyledHtml } from '../common/StyledHtml';
import Container from 'react-bootstrap/Container';
import { NoData } from '../common/NoData';

const PageContainer = styled(Container)`
  padding: 30px 0;
`;

const ONCE = [];

export const RelatedPage = ({ slug }) => {
  const [data, setData] = React.useState({});

  const fetchAndSetData = async () => {
    const pageData = await fetch(`https://ams.altius.digital/wp-json/v1/page/page/${slug}`)
      .then((response) => response.json())
      .catch((e) => notFoundObject);
    setData(pageData);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  React.useEffect(() => fetchAndSetData(), ONCE);

  const notFound = _get(data, 'data.status') === 404;

  const title = _get(data, 'title');
  const content = _get(data, 'content');

  return (
    <PageContainer>
      {notFound ? (
        <NoData />
      ) : (
        <>
          <SingleTitleArea title={title} />
          <StyledHtml dangerouslySetInnerHTML={{ __html: content }} width="100%" />
        </>
      )}
    </PageContainer>
  );
};
