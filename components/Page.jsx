import React from 'react';
import styled from '@emotion/styled';

// Components
import { Header } from './layout/header/Header';
import { Navigation } from './layout/navigation/Navigation';
import { Footer } from './layout/footer/Footer';

// Context
import { WithLayoutDataContext } from '../contexts/LayoutDataContext';

// Hooks
import { breakpoints } from '../styles/theme';

const Content = styled.div`
  min-height: 600px;
`;

const HeaderContainer = styled.div`
  @media ${breakpoints.sm} {
    position: sticky;
    top: 0;
    background-color: white;
    z-index: 999;
  }
`;

const Page = ({ children }) => {
  const [addClassName, setAddClassName] = React.useState(false);

  return (
    <WithLayoutDataContext>
      <HeaderContainer>
        <Header setAddClassName={setAddClassName} addClassName={addClassName} />
        <Navigation addClassName={addClassName} />
      </HeaderContainer>
      <Content>{children}</Content>
      <Footer />
    </WithLayoutDataContext>
  );
};

export default Page;
