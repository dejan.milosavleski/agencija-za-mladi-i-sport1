import React from 'react';
import styled from '@emotion/styled';
import _map from 'lodash/map';
import _get from 'lodash/get';
import { useMediaQuery } from 'react-responsive';
import { breakpoints } from '../../../styles/theme';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import Image from 'next/image';
import { TitleArea } from '../../common/TitleArea';

// Icons
import leftArrow from '../../../assets/icons/towards-left-arrow.svg';
import rightArrow from '../../../assets/icons/towards-right-arrow.svg';

// Context
import { LocaleContext } from '../../../contexts/LocaleContext';

const StyledImage = styled.div`
  width: 100%;
  height: 20vw;
  background: ${(props) => `linear-gradient(180deg, #00000000 0%, #000000 100%),
    url(${props.url}) no-repeat center`};
  background-size: cover;
  text-align: right;
  @media ${breakpoints.md} {
    height: 30vw;
  }
  @media ${breakpoints.sm} {
    height: 50vw;
  }
`;

const PictureText = styled.div`
  height: 100%;
  display: flex;
  flex-direction: row;
  flex-flow: column-reverse;
  color: white;
  font-family: StobiSans-Medium, sans-serif;
  padding: 20px;
`;

const TitlePicture = styled.h2`
  font-size: max(min(0.5vw, 0.5em), 2em, 2rem);
  @media ${breakpoints.xl} {
    font-size: 1.8em;
  }
  @media ${breakpoints.lg} {
    font-size: 2vw;
  }
  @media ${breakpoints.md} {
    font-size: 3.5vw;
  }
  @media ${breakpoints.sm} {
    font-size: 3.8vw;
  }
`;

const Icon = styled.div`
  :hover {
    cursor: pointer;
  }
`;

const LeftButtonCol = styled(Col)`
  text-align: end;
`;

const ButtonRow = styled(Row)`
  margin: 20px 0;
`;

const initialM = 0;

export const ProjectsSection = ({ projects = [] }) => {
  const { section_description, section_title } = projects;
  const project_items = _get(projects, 'project_items', []);
  const totalProjectItems = project_items.length;
  const [projectStart, setProjectStart] = React.useState(initialM);
  const [projectEnd, setProjectEnd] = React.useState(3);
  const { locale } = React.useContext(LocaleContext);

  const isXSmall = useMediaQuery({
    query: breakpoints.sm
  });
  const isSmall = useMediaQuery({
    query: breakpoints.md
  });

  React.useEffect(() => {
    if (isXSmall) {
      setProjectEnd(1);
    } else if (isSmall) {
      setProjectEnd(2);
    } else {
      setProjectEnd(3);
    }
  }, [isSmall, isXSmall]);

  const projectsToDisplay = project_items.slice(projectStart, projectEnd);

  const nextProjects = React.useCallback(() => {
    if (projectEnd + 1 <= totalProjectItems) {
      setProjectEnd((prevState) => prevState + 1);
      setProjectStart((prevState) => prevState + 1);
    }
  }, [projectEnd, totalProjectItems]);

  const prevProjects = React.useCallback(() => {
    if (projectStart - 1 >= 0) {
      setProjectEnd((prevState) => prevState - 1);
      setProjectStart((prevState) => prevState - 1);
    }
  }, [projectStart]);

  return (
    <div aria-labelledby="title-area">
      <TitleArea title={section_title} subtitle={section_description} />
      <Row>
        {_map(projectsToDisplay, (proj) => (
          <Col key={proj.ID} id="projects">
            <StyledImage url={proj.thumbnail}>
              <PictureText>
                <TitlePicture>
                  <Link href={`/project/${proj.post_name}`} locale={locale}>
                    {proj.title}
                  </Link>
                </TitlePicture>
              </PictureText>
            </StyledImage>
          </Col>
        ))}
        <ButtonRow>
          <LeftButtonCol onClick={prevProjects} aria-controls="projects">
            <Icon>
              <Image src={leftArrow} alt="left" width={58} height={58} />
            </Icon>
          </LeftButtonCol>
          <Col>
            <Icon onClick={nextProjects} aria-controls="projects">
              <Image src={rightArrow} alt="right" width={58} height={58} />
            </Icon>
          </Col>
        </ButtonRow>
      </Row>
    </div>
  );
};
