import React from 'react';
import _map from 'lodash/map';
import styled from '@emotion/styled';
import { theme } from '../../../styles/theme';

// Components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import { NewsNavigation } from './Navigation';
import { CardWithText } from '../../common/CardWithText';
import { StyledButton } from '../../common/StyledButton';

// Hooks
import { useTranslation } from 'next-i18next';

// Context
import { LocaleContext } from '../../../contexts/LocaleContext';

// Utils
import { goTo } from '../../../utils';

const StyledRow = styled(Row)`
  justify-content: center;
`;

const NewsContainer = styled.div`
  background-color: ${theme.palette.lightGray};
  padding: 50px 0;
`;

export const FrontPageNews = ({ content, categories }) => {
  const { t } = useTranslation('common');

  const allNewsButtonLabel = t('allNews');
  const [newsToDisplay, setNewsToDisplay] = React.useState(content);
  const [selectedCategory, setSelectedCategory] = React.useState();
  const { locale } = React.useContext(LocaleContext);

  const goToAllUrl = selectedCategory ? `/${locale}/articles/${selectedCategory.slug}` : `/${locale}/articles`;

  return (
    <NewsContainer>
      <NewsNavigation
        categories={categories}
        setNewsToDisplay={setNewsToDisplay}
        setSelectedCategory={setSelectedCategory}
        selectedCategory={selectedCategory}
      />
      <Container>
        <Row>
          {_map(newsToDisplay, (item) => {
            const { post_name, title, thumbnail, imageUrl, slug, ID } = item;
            return (
              <Col xs={12} md={4} key={ID}>
                <CardWithText
                  imageUrl={thumbnail || imageUrl}
                  title={title}
                  postName={`${selectedCategory}/${post_name || slug}`}
                  height={14}
                />
              </Col>
            );
          })}
        </Row>
        <StyledRow>
          <StyledButton minwidth={190} onClick={() => goTo(goToAllUrl)}>
            {allNewsButtonLabel}
          </StyledButton>
        </StyledRow>
      </Container>
    </NewsContainer>
  );
};
