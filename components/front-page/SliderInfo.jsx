import React from 'react';
import styled from '@emotion/styled';
import { useRouter } from 'next/router';

// Components
import Link from 'next/link';
import Carousel from 'react-bootstrap/Carousel';
import { SliderSubtitle, SliderTitle } from '../common/StyledTypography';

// Lodash
import _map from 'lodash/map';
import { breakpoints } from '../../styles/theme';

const StyledImage = styled.div`
  height: 23vw;
  width: 46vw;

  background: ${(props) => `linear-gradient(180deg, #00000000 0%, #000000 100%),
    url(${props.url}) no-repeat center`};
  background-size: cover;

  :hover {
    cursor: pointer;
  }

  @media ${breakpoints.lg} {
    min-height: 350px;
    width: 100%;
  }
`;

const StyledCarousel = styled(Carousel)`
  padding-left: 15px;
  @media ${breakpoints.lg} {
    padding-left: 0;
  }
`;

export const SliderInfo = ({ sliderInfo }) => {
  const [index, setIndex] = React.useState(0);
  const router = useRouter();
  const { locale } = router;

  const handleSelect = (selectedIndex) => {
    setIndex(selectedIndex);
  };

  return (
    <StyledCarousel activeIndex={index} onSelect={handleSelect} interval={3000}>
      {_map(sliderInfo, (si) => {
        return (
          <Carousel.Item key={si.post_name}>
            <StyledImage url={si.thumbnail} onClick={() => goTo(`${locale}/article/${si.post_name}`)}>
              <Carousel.Caption>
                <SliderTitle>
                  <Link href={`/article/${si.post_name}`}>{si.title}</Link>
                </SliderTitle>
                <SliderSubtitle>{si.excerpt}</SliderSubtitle>
              </Carousel.Caption>
            </StyledImage>
          </Carousel.Item>
        );
      })}
    </StyledCarousel>
  );
};
