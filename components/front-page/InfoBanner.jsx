import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';

// Components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Image from 'next/image';

// icons
import mLogo from '../../assets/images/background-m-logo.png';

// Lodash
import _get from 'lodash/get';
import _toUpper from 'lodash/toUpper';

const StyledContainer = styled(Container)`
  margin-bottom: 40px;
  padding: 30px 0;
`;

const StyledRow = styled(Row)``;

const LeftCol = styled(Col)`
  background-color: ${(props) => props.color};
  text-align: end;
`;

const RightCol = styled(Col)`
  background: ${(props) => `url(${props.url}) no-repeat center`};
  background-size: cover;
  text-align: end;
  min-height: 200px;
`;

const StyledImage = styled.div`
  padding-top: 15vw;
`;

const TextContainer = styled.div`
  z-index: 995;
  position: absolute;
  width: 30vw;
  text-align: left;
  margin-left: 2vw;
  @media ${breakpoints.md} {
    width: 80%;
  }
`;

const Text = styled.div`
  margin-top: 20px;

  font-size: max(min(0.5vw, 0.5em), 1.2em, 1.2rem);
  @media ${breakpoints.xl} {
    font-size: 1em;
  }
  @media ${breakpoints.lg} {
    font-size: 1.2vw;
  }
  @media ${breakpoints.md} {
    font-size: 2vw;
  }
  @media ${breakpoints.sm} {
    margin-top: 10px;
    font-size: 3.1vw;
  }
  color: white;
`;

const Title = styled.div`
  margin-top: 60px;
  font-size: max(min(0.5vw, 0.5em), 2em, 2rem);
  @media ${breakpoints.xl} {
    font-size: 1.8em;
  }
  @media ${breakpoints.lg} {
    font-size: 2vw;
  }
  @media ${breakpoints.md} {
    font-size: 3.5vw;
  }
  @media ${breakpoints.sm} {
    margin-top: 30px;
    font-size: 3.8vw;
  }
  font-family: StobiSans-Bold, sans-serif;
  color: white;
`;

export const InfoBanner = ({ contentData = {} }) => {
  const { image, text, title } = contentData;

  const backgroundColor = _get(contentData, 'background-color', theme.palette.primaryRed);

  return (
    <StyledContainer aria-labeledby="banner-title">
      <StyledRow>
        <LeftCol xs={12} md={6} color={backgroundColor}>
          <TextContainer>
            <Title id="banner-title">{_toUpper(title)}</Title>
            <Text>{text}</Text>
          </TextContainer>
          <StyledImage>
            <Image src={mLogo} alt="mlogo" width={395} height={109} />
          </StyledImage>
        </LeftCol>
        <RightCol xs={12} md={6} url={_get(image, 'url')} />
      </StyledRow>
    </StyledContainer>
  );
};
