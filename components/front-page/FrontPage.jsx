import React from 'react';
import { PublicCallsAndPromotions } from './PublicCallsAndPromotions';
import { FrontPageNews } from './news/News';
import { Campaigns } from './campaigns/Campaigns';
import { ProjectsSection } from './projects/Projects';
import { InfoBanner } from './InfoBanner';
import { FrontPagePublicCalls } from './FrontPagePublicCalls';
import { useTranslation } from 'next-i18next';
import styled from '@emotion/styled';

const HomeContainer = styled.div`
  overflow-x: hidden;
`;

export const FrontPage = ({ frontPageData, categories }) => {
  const { campaigns, projects, promo_banner, promo_content, promoted_public_calls, public_calls } = frontPageData;
  const { t } = useTranslation('common');

  const title = t('publicCallsTitle');
  const seeAll = t('seeAll');
  return (
    <HomeContainer>
      <PublicCallsAndPromotions
        promotedPublicCalls={promoted_public_calls}
        sliderInfo={promo_content}
        front
        title={title}
        seeAll={seeAll}
      />
      <FrontPageNews content={promo_content} categories={categories} />
      <Campaigns campaigns={campaigns} />
      <ProjectsSection projects={projects} />
      <InfoBanner contentData={promo_banner} />
      <FrontPagePublicCalls publicCalls={public_calls} />
    </HomeContainer>
  );
};
