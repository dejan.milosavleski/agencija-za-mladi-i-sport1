import React from 'react';
import styled from '@emotion/styled';
import { theme } from '../../styles/theme';
import _map from 'lodash/map';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { Sector } from './Sector';
import { TitleArea } from '../common/TitleArea';

const Content = styled.div`
  margin: 40px 0;
  padding: 20px 0;
  background-color: ${theme.palette.primaryRed};
`;

export const Sectors = ({ items, title, subtitle }) => {
  return (
    <Content>
      <Container>
        <TitleArea title={title} subtitle={subtitle} white />
        <Row>
          {_map(items, (sector) => (
            <Col xs={12} sm={6}>
              <Sector
                sectorName={sector.name_of_sector}
                email={sector.email_address}
                phoneNumber={sector.number}
                roleTitle={sector.position}
                leadName={sector.name_surname}
              />
            </Col>
          ))}
        </Row>
      </Container>
    </Content>
  );
};
