import React from 'react';
import styled from '@emotion/styled';

// Components
import Image from 'next/image';

// Icons
import telephone from '../../assets/icons/phone-ring-white.svg';
import envelope from '../../assets/icons/red-email-white.svg';

const Title = styled.div`
  font-size: 22px;
  font-family: StobiSans-Medium, sans-serif;
  color: white;
  border-bottom: 2px solid white;
  margin-bottom: 5px;
`;

const Text = styled.div`
  display: inline-flex;
  font-size: 16px;
  font-family: StobiSans-Light, sans-serif;
  color: white;
  line-height: 30px;
`;

const SectorContent = styled.div`
  display: flex;
  flex-direction: column;
  width: 60%;
  margin-bottom: 20px;
  margin-left: 4vw;
`;

const Icon = styled.div`
  margin-right: 4px;
  margin-top: 1px;
`;

export const Sector = ({ sectorName, roleTitle, phoneNumber, email, leadName }) => {
  return (
    <SectorContent>
      <Title>{sectorName}</Title>
      {roleTitle && <Text>{roleTitle}</Text>}
      {leadName && <Text>{leadName}</Text>}
      {phoneNumber && (
        <Text>
          <Icon>
            <Image src={telephone} alt="telephone" width={15} height={15} />
          </Icon>
          {phoneNumber}
        </Text>
      )}
      {email && (
        <Text>
          <Icon>
            <Image src={envelope} alt="envelope" width={15} height={15} />
          </Icon>
          {email}
        </Text>
      )}
    </SectorContent>
  );
};
