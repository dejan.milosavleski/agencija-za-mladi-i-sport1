import React from 'react';
import { breakpoints, theme } from '../../styles/theme';
import styled from '@emotion/styled';
import _map from 'lodash/map';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';
import { CardWithText } from '../common/CardWithText';

const Content = styled.div`
  background-color: ${theme.palette.lightGray};
  padding: 40px 0;
`;

const StyledCol = styled(Col)`
  @media ${breakpoints.sm} {
    text-align: -webkit-center;
  }
`;

export const SportAdvicePeople = ({ title, subtitle, firstLevelWorkers, secondLevelWorkers }) => {
  return (
    <Content>
      <Container aria-labelledby="title-area">
        <TitleArea title={title} subtitle={subtitle} />
        <Row>
          {_map(firstLevelWorkers, (worker) => (
            <StyledCol xs={12} sm={6} md={4}>
              <CardWithText
                imageUrl={worker.Image}
                title={worker.name_surname}
                subtitle={worker.position}
                height={10}
                smaller
              />
            </StyledCol>
          ))}
        </Row>
        <Row>
          {_map(secondLevelWorkers, (worker) => (
            <StyledCol xs={12} sm={6} md={4}>
              <CardWithText
                imageUrl={worker.Image}
                title={worker.name_surname}
                subtitle={worker.position}
                height={10}
                smaller
              />
            </StyledCol>
          ))}
        </Row>
      </Container>
    </Content>
  );
};
