import React from 'react';
import { breakpoints, theme } from '../../../styles/theme';
import { useMediaQuery } from 'react-responsive';
import { useTranslation } from 'next-i18next';
import styled from '@emotion/styled';

// Components
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Image from 'next/image';
import Link from 'next/link';
import Button from 'react-bootstrap/Button';
import { HeaderButtons } from './HeaderButtons';

// Icons and images
import macedoniaFlag from '../../../assets/icons/macedonia-flag.svg';
import albaniaFlag from '../../../assets/icons/albanian-flag.svg';
import gbFlag from '../../../assets/icons/great-britain-flag.svg';
import amsImage from '../../../assets/images/ams.png';
import hamburgerIcon from '../../../assets/icons/hamburger-icon.png';

// Contexts
import { LayoutDataContext } from '../../../contexts/LayoutDataContext';
import { LocaleContext } from '../../../contexts/LocaleContext';

// Utils
import { transformToArrayWithoutKeys } from '../../../utils';
import _map from 'lodash/map';
import _get from 'lodash/get';

export const MainRow = styled(Row)`
  justify-content: space-around;
  min-height: 118px;
`;

export const TopRow = styled(Row)`
  height: 47px;
  border-bottom: 1px solid #eceff5;
  align-items: end;
  justify-content: flex-end;
  @media ${breakpoints.md} {
    border-bottom: none;
  }
`;

export const EmptyRow = styled(Row)`
  height: 45px;
  border-bottom: 1px solid #eceff5;
  @media ${breakpoints.md} {
    border-bottom: none;
    border-top: 1px solid #eceff5;
  }
`;

export const AboutUsRow = styled(Row)`
  justify-content: end;
  @media ${breakpoints.md} {
    padding: 0;
    width: 100%;
  }
`;

const Title = styled.h1`
  padding-top: 15px;
  font-weight: 700;
  font-size: 30px;
  @media ${breakpoints.xl} {
    font-size: 22px;
    padding-top: 30px;
  }
  @media ${breakpoints.lg} {
    font-size: 18px;
    padding-top: 10px;
  }
  @media ${breakpoints.md} {
    padding-top: 40px;
    font-size: 20px;
    padding-left: 0;
    padding-right: 0;
  }
  @media ${breakpoints.sm} {
    font-size: 7vw;
    padding-top: 15px;
    text-align: center;
  }
`;

const LinkGroup = styled(ButtonGroup)`
  padding-top: 25px;
  display: flex;
  flex-direction: row;
  font-size: 18px;
  justify-content: space-around;
  color: ${theme.palette.navGray};
  padding-right: 0;

  @media ${breakpoints.xl} {
    font-size: 16px;
    padding-top: 30px;
  }
  @media ${breakpoints.lg} {
    padding-top: 10px;
  }
  @media ${breakpoints.md} {
    padding-top: 20px;
    font-size: 16px;
  }
  @media ${breakpoints.sm} {
    padding-top: 20px;
    font-size: 18px;
  }
`;
const LogoContainer = styled.div`
  padding-top: 10px;
  :hover {
    cursor: pointer;
  }
  @media ${breakpoints.xxl} {
    padding-top: 20px;
  }
  @media ${breakpoints.xl} {
    padding-top: 35px;
  }
  @media ${breakpoints.lg} {
    padding-top: 10px;
  }
  @media ${breakpoints.md} {
    padding-top: 20px;
    width: 200px;
  }
  @media ${breakpoints.sm} {
    padding-top: 10px;
    text-align: center;
    width: 170px;
  }
`;

const StyledCol = styled(Col)`
  text-align: center;
`;

const SmallColl = styled(Col)`
  display: inline-flex;
  justify-content: center;
`;

const StyledUpperCol = styled(Col)`
  text-align: end;
  padding-right: 0;
`;

const LinkButtonsCol = styled(Col)`
  padding-right: 5px;
`;

const XSmallColl = styled(Col)`
  text-align: end;
`;

const ButtonsRow = styled(Row)`
  justify-content: space-around;
`;

const Divider = styled.hr`
  border: 1px solid #eceff5;
  position: absolute;
  left: 420px;
  top: 35px;
  width: 63.5%;
  z-index: -1;
  @media ${breakpoints.lg} {
    left: 115px;
    width: 75.5%;
  }
`;

const MenuButton = styled(Button)`
  width: 40px;
  height: 35px;
  margin-top: 20px;
  background-color: white;
  border-color: ${theme.palette.primaryRed};
  margin-left: 10px;

  :hover {
    background-color: white;
    border-color: ${theme.palette.primaryRed};
  }

  :active {
    background-color: white;
    border-color: ${theme.palette.primaryRed};
    box-shadow: none;
  }
  :focus {
    background-color: white;
    border-color: ${theme.palette.primaryRed};
    box-shadow: none;
  }
`;

const IconMap = {
  en: gbFlag,
  mk: macedoniaFlag,
  sq: albaniaFlag
};

export const Header = ({ setAddClassName, addClassName }) => {
  const { headerData, footerData } = React.useContext(LayoutDataContext);
  const { side_navigation } = headerData;
  const { social_media } = footerData;
  const { t } = useTranslation('common');
  const transformedData = transformToArrayWithoutKeys(side_navigation);
  const title = t('agencyYouthAndSport');

  const { locale, changeLocale } = React.useContext(LocaleContext);
  const titleIcon = _get(IconMap, locale, macedoniaFlag);

  const handleMenuButtonClick = React.useCallback(() => {
    setAddClassName(!addClassName);
  }, [addClassName, setAddClassName]);

  const isXSmall = useMediaQuery({
    query: breakpoints.sm
  });

  const isSmall = useMediaQuery({
    query: breakpoints.md
  });

  const isMedium = useMediaQuery({
    query: breakpoints.lg
  });

  const redirectToHome = React.useCallback(() => {
    document.location.href = `/${locale}/home`;
  }, [locale]);

  return (
    <Container>
      {!isSmall ? (
        <MainRow>
          {!isMedium && (
            <Col sm={6} md={3}>
              <LogoContainer onClick={redirectToHome}>
                <Image src={amsImage} alt="ams-logo" width={236} height={83} />
              </LogoContainer>
            </Col>
          )}
          <Col md={12} lg={9}>
            <ButtonsRow>
              <Col md={4} lg={9}>
                {isMedium && (
                  <LogoContainer onClick={redirectToHome}>
                    <Image src={amsImage} alt="ams-logo" width={200} height={70} />
                  </LogoContainer>
                )}
              </Col>
              <StyledUpperCol md={8} lg={3}>
                <HeaderButtons titleIcon={titleIcon} social_media={social_media} setActiveLocale={changeLocale} />
              </StyledUpperCol>
            </ButtonsRow>
            <Divider />
            <Row>
              <Col md={7}>
                <Title>{title}</Title>
              </Col>
              <LinkButtonsCol md={5}>
                <LinkGroup>
                  {_map(transformedData, (sn) => (
                    <Link key={sn.post_name} href={`${sn.post_name}`} locale={locale}>
                      {sn.title}
                    </Link>
                  ))}
                </LinkGroup>
              </LinkButtonsCol>
            </Row>
          </Col>
        </MainRow>
      ) : (
        <Row>
          {isXSmall && (
            <>
              <Col xs={3} />
              <Col xs={6} style={{ textAlign: '-webkit-center' }}>
                <LogoContainer onClick={redirectToHome}>
                  <Image src={amsImage} alt="ams-logo" width={226} height={73} />
                </LogoContainer>
              </Col>
              <XSmallColl xs={3}>
                <MenuButton onClick={handleMenuButtonClick}>
                  <Image src={hamburgerIcon} alt="left" width={20} height={30} />
                </MenuButton>
              </XSmallColl>
            </>
          )}
          {!isXSmall && (
            <SmallColl xs={12} sm={5}>
              <LogoContainer onClick={redirectToHome}>
                <Image src={amsImage} alt="ams-logo" width={226} height={73} />
              </LogoContainer>
            </SmallColl>
          )}
          {!isXSmall && (
            <Col xs={12} sm={7}>
              <Title>{title}</Title>
            </Col>
          )}
          {!isXSmall && (
            <Col xs={12} sm={7}>
              <LinkGroup>
                {_map(transformedData, (sn) => (
                  <Link href={`${sn.post_name}`}>{sn.title}</Link>
                ))}
              </LinkGroup>
            </Col>
          )}
          <StyledCol xs={12} sm={5}>
            <HeaderButtons titleIcon={titleIcon} social_media={social_media} setActiveLocale={changeLocale} isSmall />
          </StyledCol>
        </Row>
      )}
    </Container>
  );
};
