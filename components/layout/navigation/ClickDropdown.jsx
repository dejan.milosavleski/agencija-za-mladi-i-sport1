import React from 'react';
import _map from 'lodash/map';

// Components
import Image from 'next/image';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { ArrowContainer, StyledNavDropdownContainer, StyledNavDropdownItem } from './Navigation';

// Icons
import dropdownArrow from '../../../assets/icons/dropdown-arrow.svg';

// Helpers
import { goTo, transformToArrayWithoutKeys } from '../../../utils';

export const RegularDropdown = ({ title, id, submenu, location, locale }) => {
  const [openDropdown, setOpenDropdown] = React.useState(false);

  const transformedData = transformToArrayWithoutKeys(submenu);

  const openNavigationItemDropdown = React.useCallback(() => {
    setOpenDropdown((prevState) => !prevState);
  }, []);

  const handleMainLinkPress = React.useCallback(() => goTo(`${locale}${location}`), [locale, location]);

  return (
    <StyledNavDropdownContainer>
      <StyledNavDropdownItem title={title} key={id} id={id} onClick={handleMainLinkPress} show={openDropdown}>
        {_map(transformedData, (sm) => (
          <NavDropdown.Item id={sm.ID} href={`${sm.post_name}`}>
            {sm.title}
          </NavDropdown.Item>
        ))}
      </StyledNavDropdownItem>
      <ArrowContainer onClick={openNavigationItemDropdown}>
        <Image src={dropdownArrow} width={12} height={12} alt="arrow" />
      </ArrowContainer>
    </StyledNavDropdownContainer>
  );
};
