import React from 'react';
import { breakpoints, theme } from '../../../styles/theme';
import styled from '@emotion/styled';

// Utils
import _map from 'lodash/map';
import _isNil from 'lodash/isNil';
import _isEmpty from 'lodash/isEmpty';
import { transformToArrayWithoutKeys } from '../../../utils';

// Components
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { LayoutDataContext } from '../../../contexts/LayoutDataContext';
import { LocaleContext } from '../../../contexts/LocaleContext';
import { useMediaQuery } from 'react-responsive';
import { HoverDropdown } from './HoverDropdown';
import { RegularDropdown } from './ClickDropdown';

const NavigationContainer = styled(Navbar)`
  background-color: ${theme.palette.primaryRed};
  min-height: 58px;
  font-size: 18px;
  font-family: StobiSans-Medium, sans-serif;
  .nav-link {
    color: white !important;
  }
  .dropdown-toggle::after {
    vertical-align: 1px;
    border: none;
  }
  .me-auto {
    width: 100%;
    justify-content: space-between;
  }

  .dropdown-menu {
    top: 34px;
    border: none;
  }

  @media ${breakpoints.xl} {
    font-size: 16px;
  }

  @media ${breakpoints.sm} {
    .navbar-toggler {
      display: none;
    }
  }
`;

export const StyledNavDropdownContainer = styled.div`
  display: inline-flex;
`;

export const StyledNavDropdownItem = styled(NavDropdown)`
  width: 100%;
  margin-right: 15px;
`;

export const ArrowContainer = styled.div`
  padding-top: 9px;
  right: 8px;
  position: relative;
  :hover {
    cursor: pointer;
  }
`;

const StyledContainer = styled(Container)`
  padding-right: 13px;
`;

export const Navigation = ({ addClassName }) => {
  const { headerData } = React.useContext(LayoutDataContext);
  const { locale } = React.useContext(LocaleContext);
  const { primary_menu, side_navigation } = headerData;
  const navClassName = addClassName ? 'show' : '';

  const isXSmall = useMediaQuery({
    query: breakpoints.sm
  });

  const isMedium = useMediaQuery({
    query: breakpoints.lg
  });
  const [showMap, setShowMap] = React.useState(false);

  const DropdownComponent = isMedium ? RegularDropdown : HoverDropdown;

  const transofrmedData = transformToArrayWithoutKeys(primary_menu);

  return (
    <NavigationContainer expand="lg">
      <StyledContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className={navClassName}>
          <Nav className="me-auto">
            {_map(transofrmedData, (pm) => {
              const { post_name, title, children, ID } = pm;

              return !_isEmpty(children) && !_isNil(children) ? (
                <DropdownComponent
                  key={post_name}
                  primaryMenu={primary_menu}
                  submenu={children}
                  title={title}
                  id={ID}
                  locale={locale}
                  location={post_name}
                  showMap={showMap}
                  setShowMap={setShowMap}
                />
              ) : (
                <Nav.Link key={post_name} href={`/${locale}${post_name}`}>
                  {title}
                </Nav.Link>
              );
            })}
            {isXSmall &&
              _map(transformToArrayWithoutKeys(side_navigation), (sn) => (
                <Nav.Link key={sn.post_name} href={`/${locale}${sn.post_name}`}>
                  {sn.title}
                </Nav.Link>
              ))}
          </Nav>
        </Navbar.Collapse>
      </StyledContainer>
    </NavigationContainer>
  );
};
