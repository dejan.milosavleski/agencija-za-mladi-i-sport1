import React from 'react';
import { breakpoints, theme } from '../../../styles/theme';
import styled from '@emotion/styled';
import _get from 'lodash/get';
import { useTranslation } from 'next-i18next';

// Components
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';

// Context
import { LayoutDataContext } from '../../../contexts/LayoutDataContext';
import { emailSubscriptionUrl } from '../../../contexts/constants';

const Content = styled.div`
  margin-top: 30px;
  min-height: 249px;
  background-color: ${(props) => props.color};
  @media ${breakpoints.xl} {
    padding-right: 40px;
  }
  @media ${breakpoints.lg} {
    padding-right: 0;
  }
`;

const RowContainer = styled(Row)`
  padding-top: 100px;
  @media ${breakpoints.lg} {
    padding-top: 50px;
    display: flex;
    flex-direction: column;
    text-align: center;
  }
`;

const EmailSection = styled(Col)`
  text-align: end;
  @media ${breakpoints.lg} {
    text-align: center;
    padding-top: 20px;
  }
`;

const TopFooterContainer = styled(Container)`
  height: 100%;
`;

const InputForm = styled(Form)`
  display: inline-flex;
  @media ${breakpoints.sm} {
    display: flex;
    flex-direction: column;
    margin-bottom: 10px;
    align-items: center;
  }
`;

const Feedback = styled(Form.Control.Feedback)`
  background-color: ${theme.palette.lightGray};
  border-radius: 4px;
  text-align: center;
`;

const Email = styled(Form.Control)`
  width: 300px;
  background-color: ${theme.palette.primaryRed};
  color: white;
  border-top: none;
  border-left: none;
  border-right: none;

  :focus {
    background-color: ${theme.palette.primaryRed};
    box-shadow: none;
    color: white;
  }

  ::placeholder {
    color: #b3a0a0;
  }

  @media ${breakpoints.xl} {
    width: 245px;
  }
`;

const Text = styled.h3`
  font-size: 29px;
  font-family: StobiSans-Bold, sans-serif;
  color: white;
  @media ${breakpoints.xl} {
    font-size: 24px;
  }
  @media ${breakpoints.lg} {
    font-size: 27px;
  }
  @media ${breakpoints.md} {
    font-size: 24px;
  }
`;

const SubText = styled.h5`
  font-size: 20px;
  font-weight: normal;
  color: white;
  @media ${breakpoints.xl} {
    font-size: 15px;
  }
  @media ${breakpoints.lg} {
    font-size: 17px;
  }
  @media ${breakpoints.md} {
    font-size: 15px;
  }
`;

const SubmitButton = styled(Button)`
  border-radius: 30px;
  height: 37px;
  margin-left: 25px;
  background-color: white;
  color: #474747;
  width: 150px;
  border-color: white;

  :hover {
    background-color: ${theme.palette.lightPrimaryRed};
    border-color: #cbbfbf;
  }
  :active {
    background-color: white;
    border-color: ${theme.palette.lightPrimaryRed};
    box-shadow: none;
    color: ${theme.palette.navGray};
  }
  :focus {
    background-color: white;
    border-color: ${theme.palette.lightPrimaryRed};
    box-shadow: none;
    color: ${theme.palette.navGray};
  }

  @media ${breakpoints.xl} {
    width: 120px;
  }

  @media ${breakpoints.md} {
    margin-left: 0;
  }
`;

const ValidFeedback = styled(Feedback)`
  background-color: ${theme.palette.primaryRed};
  color: white;
`;

export const EmailSubscription = () => {
  const [validated, setValidated] = React.useState(false);
  const [emailValue, setEmailValue] = React.useState('');

  const { footerData } = React.useContext(LayoutDataContext);
  const { newsletter_section } = footerData;
  const button_text = _get(newsletter_section, 'button_text');
  const placeholder = _get(newsletter_section, 'placeholder');
  const tagline = _get(newsletter_section, 'tagline');
  const text = _get(newsletter_section, 'text');

  const { t } = useTranslation('common');
  const emailValidationMessage = t('emailValidation');
  const subscriptionSuccessfulMessage = t('successfulSubscription');

  const handleEmailSubscription = React.useCallback(
    async (e) => {
      e.preventDefault();
      const form = e.currentTarget;
      if (form.checkValidity() === false) {
        e.stopPropagation();
      } else {
        try {
          await fetch(emailSubscriptionUrl, {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email: emailValue })
          });
        } catch (e) {
          console.error(e);
        }
      }

      setValidated(true);
    },
    [emailValue]
  );

  const handleEmailChange = React.useCallback((e) => {
    setEmailValue(e.target.value);
    setValidated(false);
  }, []);

  return (
    <Content color={theme.palette.primaryRed}>
      <TopFooterContainer>
        <RowContainer>
          <Col xs lg={7}>
            <Text>{tagline}</Text>
            <SubText>{text}</SubText>
          </Col>
          <EmailSection xs lg={5}>
            <InputForm noValidate validated={validated} onSubmit={handleEmailSubscription}>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Email
                  type="email"
                  placeholder={placeholder}
                  required
                  value={emailValue}
                  onChange={handleEmailChange}
                />
                <Feedback type="invalid">{emailValidationMessage}</Feedback>
                <ValidFeedback type="valid">{subscriptionSuccessfulMessage}</ValidFeedback>
              </Form.Group>
              <SubmitButton variant="primary" type="submit">
                {button_text}
              </SubmitButton>
            </InputForm>
          </EmailSection>
        </RowContainer>
      </TopFooterContainer>
    </Content>
  );
};
