import React from 'react';
import _map from 'lodash/map';
import styled from '@emotion/styled';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { CardWithText } from '../common/CardWithText';
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';

const TitleWrapper = styled.div`
  margin: 20px 0;
`;

export const RelatedNews = ({ title, content }) => {
  return (
    <Container aria-labelledby="title-area">
      <TitleWrapper>
        <TitleArea title={title} small />
      </TitleWrapper>
      <Row>
        {_map(content, (item) => {
          const { excerpt, post_name, slug, title, thumbnail, imageUrl, ID } = item;

          return (
            <Col xs={12} md={4} key={ID}>
              <CardWithText
                imageUrl={thumbnail || imageUrl}
                title={title}
                subtitle={excerpt}
                postName={post_name || slug}
                white
                height={14}
              />
            </Col>
          );
        })}
      </Row>
    </Container>
  );
};
