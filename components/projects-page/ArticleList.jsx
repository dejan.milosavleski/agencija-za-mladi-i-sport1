import React from 'react';
import styled from '@emotion/styled';

// Components
import { TitleArea } from '../common/TitleArea';
import Container from 'react-bootstrap/Container';
import { ProjectCampaignPreview } from './ProjectCampaignPreview';
import { ScrollEndMessage } from '../common/ScrollEndMessage';
import { LoaderMessage } from '../common/LoaderMessage';
import { StyledInfiniteScroll } from '../common/StyledElements';

// Lodash
import _map from 'lodash/map';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

const StyledContainer = styled(Container)`
  margin-top: 40px;
  margin-bottom: 40px;
`;

export const ArticleList = ({ articles, areaTitle, areaSubtitle, campaign, fetchMoreArticlesUrl }) => {
  const [itemsToRender, setItemsToRender] = React.useState(articles);
  const [hasMoreItems, setHasMoreItems] = React.useState(true);
  const [page, setPage] = React.useState(2);

  const fetchMoreArticles = async () => {
    let res;

    res = await fetch(fetchMoreArticlesUrl(page));

    const items = await res.json();

    const updatedNews = itemsToRender.concat(items);
    setItemsToRender(updatedNews);
    setPage(page + 1);
    setHasMoreItems(!_isEmpty(items));
  };

  return (
    <StyledContainer aria-labeledby="title-area">
      <TitleArea title={areaTitle} subtitle={areaSubtitle} />
      <StyledInfiniteScroll
        dataLength={_get(itemsToRender, 'length', 0)}
        next={fetchMoreArticles}
        hasMore={hasMoreItems}
        loader={<LoaderMessage />}
        endMessage={<ScrollEndMessage />}
      >
        {_map(articles, (item) => (
          <ProjectCampaignPreview
            projectImages={item.gallery}
            text={item.excerpt}
            title={item.title}
            postName={item.slug}
            imageUrl={item.imageUrl}
            campaign={campaign}
          />
        ))}
      </StyledInfiniteScroll>
    </StyledContainer>
  );
};
