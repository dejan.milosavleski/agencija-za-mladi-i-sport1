import React from 'react';
import styled from '@emotion/styled';
import { useRouter } from 'next/router';
import { useTranslation } from 'next-i18next';

// Components
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Carousel from 'react-bootstrap/Carousel';
import { StyledButton } from '../common/StyledButton';
import Link from 'next/link';

// Utils
import _map from 'lodash/map';
import _isEmpty from 'lodash/isEmpty';
import { goTo } from '../../utils';
import { breakpoints } from '../../styles/theme';

// Context
import { LocaleContext } from '../../contexts/LocaleContext';

const RightCol = styled(Col)`
  align-self: center;
  margin-left: 2vw;
  @media ${breakpoints.md} {
    margin-left: 0;
  }
`;

const StyledRow = styled(Row)`
  padding-right: 20px;
`;

const Title = styled.div`
  font-size: 2.2vw;
  font-family: StobiSans-Medium, sans-serif;
  @media ${breakpoints.lg} {
    font-size: 2.5vw;
  }
  @media ${breakpoints.md} {
    font-size: 4.95vw;
  }
`;

const Text = styled.div`
  padding-top: 10px;
  padding-bottom: 10px;
  font-size: 0.95vw;
  @media ${breakpoints.xl} {
    font-size: 1.5vw;
  }
  @media ${breakpoints.md} {
    font-size: 2.95vw;
  }
`;

const CarouselItem = styled(Carousel.Item)`
  width: 100%;
  text-align: left;
`;

const StyledImage = styled.img`
  width: 100%;
  height: 23vw;

  :hover {
    cursor: ${(props) => (props.campaign === 'campaign' ? 'pointer' : 'normal')};
  }

  @media ${breakpoints.lg} {
    height: 23vw;
  }
  @media ${breakpoints.md} {
    height: 40vw;
  }
`;

const MainRow = styled(Row)`
  margin-bottom: 20px;
`;

export const ProjectCampaignPreview = ({ projectImages, title, text, postName, imageUrl, campaign }) => {
  const { t } = useTranslation('common');

  const buttonLabel = t('findOutMore');

  const { locale } = React.useContext(LocaleContext);
  const router = useRouter();
  const { pathname } = router;
  const editedPathname = pathname.slice(0, -1);

  const handleImageClick = () => {
    if (campaign) {
      goTo(`${locale}${editedPathname}/${postName}`);
    }
  };

  return (
    <MainRow>
      <Col sm={12} md={6}>
        {!_isEmpty(projectImages) ? (
          <Carousel nextIcon={''} prevIcon={''} aria-controls="carousel-item">
            {_map(projectImages, (si) => (
              <CarouselItem id="carousel-item">
                <StyledImage
                  src={si}
                  alt="sliderImage"
                  campaign={campaign ? 'campaign' : ''}
                  onClick={handleImageClick}
                />
              </CarouselItem>
            ))}
          </Carousel>
        ) : (
          <StyledImage
            src={imageUrl}
            alt="sliderImage"
            campaign={campaign ? 'campaign' : ''}
            onClick={handleImageClick}
          />
        )}
      </Col>
      <RightCol sm={12} md={5}>
        <StyledRow>
          <Title>
            <Link href={`${editedPathname}/${postName}`}>{title}</Link>
          </Title>
        </StyledRow>
        <StyledRow>
          <Text>{text}</Text>
        </StyledRow>
        <StyledRow>
          <StyledButton minwidth={150} marginleft={15} onClick={() => goTo(`/${locale}${editedPathname}/${postName}`)}>
            {buttonLabel}
          </StyledButton>
        </StyledRow>
      </RightCol>
    </MainRow>
  );
};
