import React from 'react';
import styled from '@emotion/styled';
import _isEmpty from 'lodash/isEmpty';
import _flatten from 'lodash/flatten';
// Components
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Categories } from '../common/categories/Categories';
import { TitleArea } from '../common/TitleArea';
import { ActiveDoneController } from './ActiveDoneController';
import { InfinityItems } from './InfinityItems';

// Contexts
import { LocaleContext } from '../../contexts/LocaleContext';

// Utils
import { getCategoryTitle } from '../../utils';
import _find from 'lodash/find';

const CategoryLabel = styled.div`
  font-size: 32px;
  font-family: StobiSans-Medium, sans-serif;
  text-align: center;
`;

export const PublicInfoAndCalls = ({
  title,
  firstPageName,
  categories,
  currentCategory,
  categoriesPageName,
  isCalls,
  fixedUrl,
  fetchMoreItemsUrl,
  fetchMoreItemsFunctionUrl
}) => {
  React.useEffect(async () => {
    const pagename = isCalls ? 'public-calls' : 'public-infos';
    const fetchUrl = currentCategory
      ? `https://ams.altius.digital/wp-json/v1/${pagename}?status=1&limit=10&page=1&category=${currentCategory}`
      : fixedUrl;

    const res = await fetch(fetchUrl);
    const publicCalls = await res.json();

    setItemsToRender(publicCalls);
  }, [currentCategory, fixedUrl, isCalls]);

  const allSubCategories = _flatten(
    categories.map((cat) => {
      let children = [];
      if (!_isEmpty(cat.children)) {
        children = cat.children;
      }
      return [...children];
    })
  );

  const allCategories = [...categories, ...allSubCategories];
  const currentCategoryObject = _find(allCategories, (cat) => cat.slug === currentCategory);
  const categoryTitle = getCategoryTitle(currentCategoryObject);
  const [active, setActive] = React.useState(1);
  const [itemsToRender, setItemsToRender] = React.useState([]);
  const { locale } = React.useContext(LocaleContext);

  return (
    <Container>
      <TitleArea title={title} />
      {currentCategory && (
        <Row>
          <CategoryLabel>{categoryTitle}</CategoryLabel>
        </Row>
      )}
      {isCalls && (
        <Row>
          <ActiveDoneController
            active={active}
            setActive={setActive}
            setItemsToRender={setItemsToRender}
            category={currentCategory}
          />
        </Row>
      )}
      <Row>
        <Col xs={12} md={3}>
          <Categories
            firstPageName={categoriesPageName}
            currentCategory={currentCategory}
            currentSubcategory={''}
            categories={categories}
            locale={locale}
            currentCategoryObject={currentCategoryObject}
          />
        </Col>
        <Col xs={12} md={9} id="public-calls-and-info">
          <InfinityItems
            fetchMoreItemsUrl={fetchMoreItemsUrl}
            fetchMoreItemsFunctionUrl={fetchMoreItemsFunctionUrl}
            initialItems={itemsToRender}
            firstPageName={firstPageName}
            status={active}
          />
        </Col>
      </Row>
    </Container>
  );
};
