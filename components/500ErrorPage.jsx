import React from 'react';
import { useTranslation } from 'next-i18next';
import { ErrorPage } from './ErrorPage';

export const ServerErrorPage = () => {
  const { t } = useTranslation('common');

  const serverErrorMessage = t('serverError');
  return <ErrorPage code={500} message={serverErrorMessage} />;
};
