import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';
import { useRouter } from 'next/router';

// Components
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Link from 'next/link';
import { PageTitle } from '../common/StyledTypography';
import { SingleItemMainArea } from './SingleItemMainArea';
import { NoData } from '../common/NoData';

// utils
import _map from 'lodash/map';
import _filter from 'lodash/filter';
import _get from 'lodash/get';
import { getLevelPath } from '../../utils';
import { notFoundObject } from '../../contexts/constants';

const ImageContainer = styled.div`
  width: 100%;
  margin-bottom: ${(props) => props.bottom}px;
`;

const PageContainer = styled(Container)`
  margin-top: 30px;
  margin-bottom: 30px;
`;

const Title = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
  font-family: StobiSans-Medium, sans-serif;
  @media ${breakpoints.lg} {
    margin-top: 30px;
  }
`;

const ItemTitle = styled.div`
  color: ${theme.palette.navGray};
  font-size: 22px;
  font-family: StobiSans-Bold, sans-serif;
  margin-bottom: 20px;
`;

const Divider = styled.hr`
  border: 3px solid ${theme.palette.primaryRed};
  width: 60px;
  opacity: 1;
`;

const SmallImage = styled.img`
  max-width: 300px;
`;

const StyledCol = styled(Col)`
  padding-right: 50px;
`;

const RelatedNewsCol = styled(Col)`
  @media ${breakpoints.lg} {
    text-align: -webkit-center;
  }
`;

export const SingleItemPage = ({ title, rightSectionTitle, rightSectionItems, slug, fetchUrl }) => {
  const { pathname } = useRouter();
  const path = getLevelPath(pathname, 1);
  const [item, setItem] = React.useState({});

  const withFilteredCurrent = _filter(rightSectionItems, (ri) => ri.slug !== item.slug);

  const fetchAndSetData = async () => {
    const pageData = await fetch(`${fetchUrl}${slug}`)
      .then((response) => response.json())
      .catch((e) => notFoundObject);
    setItem(pageData);
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  React.useEffect(() => fetchAndSetData(), [slug]);

  const notFound = _get(item, 'data.status') === 404;

  const { page_title, imageUrl, content } = item;

  return (
    <PageContainer>
      <PageTitle>{title}</PageTitle>
      <Row>
        <StyledCol xs={12} lg={9}>
          {notFound ? <NoData /> : <SingleItemMainArea pageTitle={page_title} imageUrl={imageUrl} content={content} />}
        </StyledCol>
        <RelatedNewsCol xs={12} lg={3}>
          <Title>{rightSectionTitle}</Title>
          <Divider />
          {_map(withFilteredCurrent, (filteredItem) => (
            <>
              <ImageContainer bottom={20}>
                <SmallImage src={filteredItem.imageUrl} />
              </ImageContainer>
              <ItemTitle>
                <Link href={`/${path}/${filteredItem.slug}`}>{filteredItem.title}</Link>
              </ItemTitle>
            </>
          ))}
        </RelatedNewsCol>
      </Row>
    </PageContainer>
  );
};
