import React from 'react';
import { SingleTitleArea } from './SingleTitleArea';
import { StyledHtml } from '../common/StyledHtml';
import styled from '@emotion/styled';

export const ImageContainer = styled.div`
  width: 100%;
  margin-bottom: ${(props) => props.bottom}px;
`;

export const SingleItemMainArea = ({ pageTitle, imageUrl, content }) => {
  return (
    <>
      <SingleTitleArea title={pageTitle} />
      <ImageContainer bottom={50}>
        <img src={imageUrl} alt="picture" style={{ width: '100%' }} />
      </ImageContainer>
      <StyledHtml dangerouslySetInnerHTML={{ __html: content }} width="100%" />
    </>
  );
};
