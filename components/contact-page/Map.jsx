import React from 'react';
import GoogleMapReact from 'google-map-react';

const SimpleMap = ({ latitude, longitude }) => {
  const roundedLat = Math.round(latitude * 100) / 100;
  const roundedLng = Math.round(longitude * 100) / 100;

  const renderMarkers = (map, maps) => {
    let marker = new maps.Marker({
      position: { lat: roundedLat, lng: roundedLng },
      map,
      title: 'Exact location'
    });
  };
  return (
    <GoogleMapReact
      bootstrapURLKeys={{ key: 'AIzaSyCSGGQgWqbjzEVOU1QIBpN7XeFmS5xxvtw' }}
      defaultCenter={{ lat: roundedLat, lng: roundedLng }}
      defaultZoom={16}
      onGoogleApiLoaded={({ map, maps }) => renderMarkers(map, maps)}
    />
  );
};

export default SimpleMap;
