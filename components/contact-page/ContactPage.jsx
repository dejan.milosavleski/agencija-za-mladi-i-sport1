import React from 'react';
import styled from '@emotion/styled';
import _map from 'lodash/map';

// Components
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';
import { ContactPageInfoSection } from './ContactPageSection';
import Map from './Map';

// Hooks
import { useTranslation } from 'next-i18next';
import { breakpoints } from '../../styles/theme';

const StyledRow = styled(Row)`
  margin-bottom: 30px;
`;

const MainRow = styled(Row)`
  margin-top: 50px;
`;

const StyledCol = styled(Col)`
  @media ${breakpoints.sm} {
    margin-top: 50px;
  }
`;

export const ContactPage = ({ data }) => {
  const { t } = useTranslation('common');

  const pageTitle = t('contactPageTitle');
  const { section, latitude, longitude } = data;

  return (
    <Container>
      <TitleArea title={pageTitle} />
      <MainRow>
        <Col sm={12} md={6}>
          <Map latitude={latitude} longitude={longitude} />
        </Col>
        <StyledCol sm={12} md={6}>
          {_map(section, (sec) => (
            <StyledRow key={section.title_section}>
              <ContactPageInfoSection item={sec} />
            </StyledRow>
          ))}
        </StyledCol>
      </MainRow>
    </Container>
  );
};
