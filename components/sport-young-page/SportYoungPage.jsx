import React from 'react';
import styled from '@emotion/styled';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';

// Components
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import { TitleArea } from '../common/TitleArea';
import { SliderAndNews } from './SliderAndNews';
import { RelatedPages } from './RelatedPages';
import { InfoBanner } from '../front-page/InfoBanner';
import { RelatedNews } from '../projects-page/RelatedNews';
import { StyledButton } from '../common/StyledButton';
import { ProjectsSection } from '../front-page/projects/Projects';
import { NoData } from '../common/NoData';

// Utils
import { goTo } from '../../utils';
import { notFoundObject } from '../../contexts/constants';
import _get from 'lodash/get';
import _isEmpty from 'lodash/isEmpty';

const PageContainer = styled(Container)`
  padding-top: 30px;
  padding-bottom: 30px;
`;

const StyledRow = styled(Row)`
  justify-content: space-evenly;
`;

export const SportYoungPage = ({ slug }) => {
  const [content, setContent] = React.useState({});
  const [relatedNews, setRelatedNews] = React.useState([]);
  const { t } = useTranslation('common');
  const { projects, promo_banner, promo_content, promoted_public_calls, related_pages, title, category } = content;

  const router = useRouter();
  const { locale } = router;

  const fetchAndSetData = async () => {
    const pageData = await fetch(`https://ams.altius.digital/wp-json/v1/pages/${slug}`)
      .then((response) => response.json())
      .catch((e) => notFoundObject);
    setContent(pageData);
  };

  const fetchAndSetRelatedNews = async (newsCategory) => {
    const relatedNews = await fetch(
      `https://ams.altius.digital/wp-json/v1/posts/?limit=3&page=1&category=${newsCategory}`
    )
      .then((response) => response.json())
      .catch((e) => notFoundObject);
    setRelatedNews(relatedNews);
  };

  React.useEffect(() => {
    if (_isEmpty(content)) {
      fetchAndSetData();
    }
    if (_isEmpty(relatedNews) && !_isEmpty(content)) {
      fetchAndSetRelatedNews(_get(content, 'category', 'all'));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [content, relatedNews, slug]);

  const notFound = _get(content, 'data.status') === 404;

  const allNewsButtonLabel = t('allNews');
  const relatedNewsLabel = t('relatedNews');

  return notFound ? (
    <PageContainer>
      <NoData />
    </PageContainer>
  ) : (
    <>
      <TitleArea title={title} />
      <PageContainer>
        <SliderAndNews sliderInfo={promo_content} news={promoted_public_calls} category={category} locale={locale} />
        <RelatedPages relatedPages={related_pages} locale={locale} />
        <InfoBanner contentData={promo_banner} />
        <ProjectsSection projects={projects} />
        <RelatedNews content={relatedNews} title={relatedNewsLabel} />
        <StyledRow>
          <StyledButton minwidth={190} onClick={() => goTo(`/${locale}/articles/${category}`)}>
            {allNewsButtonLabel}
          </StyledButton>
        </StyledRow>
      </PageContainer>
    </>
  );
};
