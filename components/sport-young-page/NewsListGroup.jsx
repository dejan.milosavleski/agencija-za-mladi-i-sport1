import React from 'react';
import styled from '@emotion/styled';
import { breakpoints, theme } from '../../styles/theme';
import _map from 'lodash/map';
import { useTranslation } from 'next-i18next';

// Components
import ListGroup from 'react-bootstrap/ListGroup';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { Divider, Title } from '../common/public-calls-and-promotions/PromotedPublicCallsContent';
import Image from 'react-bootstrap/Image';
import Link from 'next/link';

const StyledContent = styled.div`
  margin: 15px 0;
`;

const StyledItem = styled.div`
  font-family: StobiSans-Medium, sans-serif;
  font-size: 22px;
  @media ${breakpoints.md} {
    margin-top: 20px;
  }
`;

const StyledRow = styled(Row)`
  border-bottom: 1px solid ${theme.palette.mediumGray};
`;

const NewsTitle = styled.div`
  font-family: StobiSans-Bold, sans-serif;
  font-size: 16px;
  padding-top: 20px;
`;

const StyledLinkLabel = styled.div`
  font-size: 16px;
  width: 100%;
  text-align: center;
  margin-top: 10px;
  text-decoration: underline;
`;

export const NewsListGroup = ({ news, title, category, locale }) => {
  const { t } = useTranslation('common');

  const seeAllLabel = t('seeAll');
  return (
    <ListGroup aria-labeledby="title">
      <StyledItem>
        <Title id="title">{title}</Title>
        <Divider />
      </StyledItem>
      {_map(news, (newsItem) => (
        <StyledRow>
          <Col xs={12} sm={4}>
            <StyledContent>
              <Image src={newsItem.imageUrl} fluid />
            </StyledContent>
          </Col>
          <Col xs={12} sm={8}>
            <NewsTitle>
              <Link href={`/articles/${news.slug}`} locale={locale}>
                {newsItem.title}
              </Link>
            </NewsTitle>
          </Col>
        </StyledRow>
      ))}
      <StyledLinkLabel>
        <Link href={`/articles/${category}`}>{seeAllLabel}</Link>
      </StyledLinkLabel>
    </ListGroup>
  );
};
