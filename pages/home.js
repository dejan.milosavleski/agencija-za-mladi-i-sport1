import React from 'react';
import { frontPageDataUrl, newsCategoryDataUrl } from '../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../500Error';
import { FrontPage } from '../components/front-page/FrontPage';

function Home({ frontPageData, categories }) {
  return <FrontPage frontPageData={frontPageData} categories={categories} />;
}
export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(frontPageDataUrl);
    const res2 = await fetch(newsCategoryDataUrl);
    const frontPageData = await res.json();
    const categories = await res2.json();

    return {
      props: {
        frontPageData,
        categories: categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        frontPageData: errorData,
        categories: [],
        ...translationData
      }
    };
  }
}

export default Home;
