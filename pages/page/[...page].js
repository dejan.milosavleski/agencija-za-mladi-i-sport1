import React from 'react';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { RelatedPage } from '../../components/related-page/RelatedPage';
import { useRouter } from 'next/router';
import _get from 'lodash/get';

export default function Page() {
  const router = useRouter();
  const slug = router.query.page || [];
  const firstSlug = _get(slug, '[0]');

  return <RelatedPage slug={firstSlug} />;
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  return {
    props: {
      ...translationData
    }
  };
}
