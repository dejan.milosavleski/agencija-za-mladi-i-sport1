import React from 'react';
import { ContactPage } from '../components/contact-page/ContactPage';
import { contactDataUrl } from '../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../500Error';
import Head from 'next/head';

export default function Contact({ data }) {
  return (
    <>
      <Head title="ContactPage">
        <script src="https://npmcdn.com/google-map-react@1.0.1/dist/GoogleMapReact.js"></script>
        <title>contact page</title>
      </Head>
      <ContactPage data={data} />
    </>
  );
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(contactDataUrl);
    const contact = await res.json();

    return {
      props: {
        data: contact,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        data: errorData,
        ...translationData
      }
    };
  }
}
