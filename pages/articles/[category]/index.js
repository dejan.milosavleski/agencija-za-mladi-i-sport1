import React from 'react';
import { NewsAndCategories } from '../../../components/news-page/NewsAndCategories';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../../500Error';

export default function News({ news, categories, currentCategory }) {
  const firstTwoNews = news.slice(0, 2);
  const restOfNews = news.slice(2, news.length - 1);

  return (
    <NewsAndCategories
      firstTwoNews={firstTwoNews}
      categories={categories}
      currentCategory={currentCategory}
      firstPageName="articles"
      restOfNews={restOfNews}
    />
  );
}

export async function getStaticPaths({ locales }) {
  let categories = [];

  const res = await fetch(`https://ams.altius.digital/wp-json/v1/categories/category`);
  categories = await res.json();

  const paths = [];
  categories.forEach((category) => {
    locales.forEach((wantedLocale) => {
      const path = {
        params: { category: category.slug },
        locale: wantedLocale
      };
      paths.push(path);
    });
  });

  return { paths, fallback: false };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const { category } = params;
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/posts/?limit=8&page=1&category=${category}`);
    const res2 = await fetch(`https://ams.altius.digital/wp-json/v1/categories/category`);
    const news = await res.json();
    const categories = await res2.json();

    return {
      props: {
        news,
        categories,
        currentCategory: category,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        news: errorData,
        ...translationData
      }
    };
  }
}
