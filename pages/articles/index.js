import React from 'react';
import { NewsAndCategories } from '../../components/news-page/NewsAndCategories';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../500Error';

export default function NewsAll({ news, categories }) {
  const firstTwoNews = news.slice(0, 2);
  const restOfNews = news.slice(2, news.length - 1);

  return (
    <NewsAndCategories
      firstTwoNews={firstTwoNews}
      categories={categories}
      currentCategory="all"
      firstPageName="articles"
      restOfNews={restOfNews}
    />
  );
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/posts/?limit=8&page=1&category=all`);
    const res2 = await fetch(`https://ams.altius.digital/wp-json/v1/categories/category`);
    const news = await res.json();
    const categories = await res2.json();

    return {
      props: {
        news,
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        news: errorData,
        ...translationData
      }
    };
  }
}
