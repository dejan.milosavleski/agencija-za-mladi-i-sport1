import React from 'react';
import { SingleItemPublic } from '../../components/public-info-and-calls-page/SingleItemPublic';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import _get from 'lodash/get';

const publicCallFetchUrl = 'https://ams.altius.digital/wp-json/v1/public-call/';

export default function PubilcCall() {
  const { t } = useTranslation('common');
  const pageTitle = t('publicCalls');
  const router = useRouter();
  const slug = router.query.slug || [];
  const firstSlug = _get(slug, '[0]');

  return <SingleItemPublic title={pageTitle} fetchUrl={publicCallFetchUrl} slug={firstSlug} isPublicCall />;
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  return {
    props: {
      ...translationData
    }
  };
}
