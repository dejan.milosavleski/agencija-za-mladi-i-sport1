import React from 'react';
import { campaignsDataUrl } from '../../contexts/constants';
import { SingleItemPage } from '../../components/single-item-page/SingleItemPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../../500Error';
import _get from 'lodash/get';
import { useRouter } from 'next/router';

const campaignFetchUrl = 'https://ams.altius.digital/wp-json/v1/campaign/';

export default function Campaigns({ campaign, campaigns }) {
  const { t } = useTranslation('common');

  const pageTitle = t('campaign');
  const rightSectionTitle = t('restOfCampaigns');
  const router = useRouter();
  const slug = router.query.slug || [];
  const firstSlug = _get(slug, '[0]');

  return (
    <SingleItemPage
      title={pageTitle}
      item={campaign}
      rightSectionTitle={rightSectionTitle}
      rightSectionItems={campaigns}
      slug={firstSlug}
      fetchUrl={campaignFetchUrl}
    />
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ params, locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const res = await fetch(`https://ams.altius.digital/wp-json/v1/campaign/${params.slug}`);
    const res2 = await fetch(campaignsDataUrl);
    const campaigns = await res2.json();

    return {
      props: {
        campaigns,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        campaign: errorData,
        ...translationData
      }
    };
  }
}
