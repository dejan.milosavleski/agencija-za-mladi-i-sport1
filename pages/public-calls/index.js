import React from 'react';
import { publicCallsCategoryDataUrl, publicCallsDataUrl } from '../../contexts/constants';
import { PublicInfoAndCalls } from '../../components/public-info-and-calls-page/PublicInfoAndCalls';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../500Error';
import { useTranslation } from 'next-i18next';

export default function PublicCalls({ publicCalls, categories }) {
  const fetchMoreItemsUrl = (page, status) =>
    `https://ams.altius.digital/wp-json/v1/public-calls?status=${status}&limit=10&page=${page}`;

  const { t } = useTranslation('common');
  const pageTitle = t('publicCalls');

  return (
    <PublicInfoAndCalls
      categories={categories}
      currentCategory=""
      firstPageName="public-call"
      categoriesPageName="public-calls"
      items={publicCalls}
      isCalls
      fixedUrl={publicCallsDataUrl}
      title={pageTitle}
      fetchMoreItemsFunctionUrl={fetchMoreItemsUrl}
    />
  );
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res2 = await fetch(publicCallsCategoryDataUrl);
    const categories = await res2.json();

    return {
      props: {
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        categories: errorData,
        ...translationData
      }
    };
  }
}
