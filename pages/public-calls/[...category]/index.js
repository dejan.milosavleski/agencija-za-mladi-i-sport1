import React from 'react';
import { PublicInfoAndCalls } from '../../../components/public-info-and-calls-page/PublicInfoAndCalls';
import { publicCallsCategoryDataUrl } from '../../../contexts/constants';
import { useRouter } from 'next/router';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import errorData from '../../../500Error';
import { useTranslation } from 'next-i18next';
import _get from 'lodash/get';

export default function PublicCalls({ categories }) {
  const router = useRouter();
  const { category } = router.query;

  const fetchMoreItemsUrl = (page, status) =>
    `https://ams.altius.digital/wp-json/v1/public-calls?status=${status}&limit=10&page=${page}&category=${category}`;

  const { t } = useTranslation('common');
  const pageTitle = t('publicCalls');

  return (
    <PublicInfoAndCalls
      categories={categories}
      currentCategory={_get(category, '[0]')}
      firstPageName="public-call"
      categoriesPageName="public-calls"
      isCalls
      title={pageTitle}
      fetchMoreItemsFunctionUrl={fetchMoreItemsUrl}
    />
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res2 = await fetch(publicCallsCategoryDataUrl);
    const categories = await res2.json();

    return {
      props: {
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        categories: errorData,
        ...translationData
      }
    };
  }
}
