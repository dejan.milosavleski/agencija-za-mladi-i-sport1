import React from 'react';
import { ArticleList } from '../../components/projects-page/ArticleList';
import { campaignsDataUrl } from '../../contexts/constants';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../../500Error';

export default function Campaigns({ campaigns }) {
  const { t } = useTranslation();

  const areaTitle = t('campaigns');
  const areaSubtitle = t('lookAtCampaignsText');
  const fetchMoreArticlesUrl = (page) => `https://ams.altius.digital/wp-json/v1/campaigns?limit=10&page=${page}`;
  return (
    <div>
      <ArticleList
        articles={campaigns}
        areaTitle={areaTitle}
        areaSubtitle={areaSubtitle}
        campaign
        fetchMoreArticlesUrl={fetchMoreArticlesUrl}
      />
    </div>
  );
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res = await fetch(campaignsDataUrl);
    const campaigns = await res.json();

    return {
      props: {
        campaigns,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        campaigns: errorData,
        ...translationData
      }
    };
  }
}
