import React from 'react';
import { ErrorPage } from '../components/ErrorPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';

export default function NotFound() {
  const { t } = useTranslation('common');

  const pageNotFoundLabel = t('pageNotFound');
  return <ErrorPage code={404} message={pageNotFoundLabel} />;
}

export async function getStaticProps({ params, locale }) {
  try {
    return {
      props: {
        ...(await serverSideTranslations(locale, ['common']))
      }
    };
  } catch (e) {
    return {
      props: {}
    };
  }
}
