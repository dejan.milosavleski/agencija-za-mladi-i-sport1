import React from 'react';
import { SingleItemPublic } from '../../components/public-info-and-calls-page/SingleItemPublic';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import { useRouter } from 'next/router';
import _get from 'lodash/get';

const publicInformationFetchUrl = 'https://ams.altius.digital/wp-json/v1/public-info/';

export default function PubilcInformation() {
  const { t } = useTranslation('common');
  const pageTitle = t('publicInformation');
  const router = useRouter();
  const slug = router.query.slug || [];
  const firstSlug = _get(slug, '[0]');

  return <SingleItemPublic title={pageTitle} fetchUrl={publicInformationFetchUrl} slug={firstSlug} />;
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  return {
    props: {
      ...translationData
    }
  };
}
