import React from 'react';
import { useRouter } from 'next/router';
import { SingleNewsItem } from '../../components/news-page/single-news-item/SingleNewsItem';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import _get from 'lodash/get';
import errorData from '../../500Error';

export default function SingleNewsArticle({ categories }) {
  const router = useRouter();
  const { category } = router.query;
  const slug = router.query.id || [];
  const firstSlug = _get(slug, '[0]');

  return <SingleNewsItem categories={categories} currentCategory={category} slug={firstSlug} />;
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);
  try {
    const res2 = await fetch(encodeURI(`https://ams.altius.digital/wp-json/v1/categories/category`));
    const categories = await res2.json();

    return {
      props: {
        categories,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        newsItem: errorData,
        ...translationData
      }
    };
  }
}
