import React from 'react';
import { SportYoungPage } from '../../components/sport-young-page/SportYoungPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useRouter } from 'next/router';
import _get from 'lodash/get';

export default function Pages() {
  const router = useRouter();
  const slug = router.query.pages || [];
  const firstSlug = _get(slug, '[0]');

  return <SportYoungPage slug={firstSlug} />;
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  return {
    props: {
      ...translationData
    }
  };
}
