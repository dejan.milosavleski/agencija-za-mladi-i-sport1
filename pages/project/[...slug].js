import React from 'react';
import { projectsDataUrl } from '../../contexts/constants';
import { SingleItemPage } from '../../components/single-item-page/SingleItemPage';
import { serverSideTranslations } from 'next-i18next/serverSideTranslations';
import { useTranslation } from 'next-i18next';
import errorData from '../../500Error';
import _get from 'lodash/get';
import { useRouter } from 'next/router';

const projectFetchUrl = 'https://ams.altius.digital/wp-json/v1/project/';

export default function SingleProject({ campaign, campaigns }) {
  const { t } = useTranslation('common');

  const pageTitle = t('project');
  const rightSectionTitle = t('restOfProjects');
  const router = useRouter();
  const slug = router.query.slug || [];
  const firstSlug = _get(slug, '[0]');

  return (
    <SingleItemPage
      title={pageTitle}
      item={campaign}
      rightSectionTitle={rightSectionTitle}
      rightSectionItems={campaigns}
      slug={firstSlug}
      fetchUrl={projectFetchUrl}
    />
  );
}

export async function getStaticPaths() {
  return {
    paths: [],
    fallback: 'blocking'
  };
}

export async function getStaticProps({ locale }) {
  const translationData = await serverSideTranslations(locale, ['common']);

  try {
    const res2 = await fetch(projectsDataUrl);
    const campaigns = await res2.json();

    return {
      props: {
        campaigns,
        ...translationData
      }
    };
  } catch (e) {
    return {
      props: {
        campaign: errorData,
        ...translationData
      }
    };
  }
}
