const errorData = {
  code: 500,
  message: 'An error occurred during static props generation.'
};

export default errorData;
