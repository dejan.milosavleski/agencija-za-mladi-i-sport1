export const theme = {
  palette: {
    white: '#FFFFFF',
    primaryRed: '#D8232A',
    lightPrimaryRed: '#e0484f',
    secondaryRed: '#C50000',
    ternaryRed: '#c8102e',
    navGray: '#464646',
    lightGray: '#FAFAFA',
    mediumGray: '#A4A9B2',
    primaryGray: '#474747'
  }
};

const size = {
  sm: '576px',
  md: '766px',
  lg: '992px',
  xl: '1300px',
  xxl: '1400px'
};

export const breakpoints = {
  sm: `(max-width: ${size.sm})`,
  md: `(max-width: ${size.md})`,
  lg: `(max-width: ${size.lg})`,
  xl: `(max-width: ${size.xl})`,
  xxl: `(max-width: ${size.xxl})`
};
